Principios de diseño de software - practica 1

El objetivo de esta aplicación es recolectar los datos del usuario, luego escribirla a un archivo XML para finalizar agregando su firma electronica.

La funcionalidad actualmente consta de la función "ingresarPersona()" que recoge los datos del usuario y la función "generarXML()" que serializa la clase persona y la escribe a un archivo XML.

Para la siguiente versión se implementará la parte de la generación de la firma electronica dentro el archivo XML utilizando un certificado digital.

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Las debilidades identificadas en en la tarea "Identificación de conceptos" fueron:
    - Comentarios excesivos y nada aclaratorios para la funcionalidad de la aplicación.
    - Clase Persona declarada en la funcion main.
    - Mala declaracion de nombres de las propiedades de la clase Persona.
    - Nombres de metodos no siguen la convencion de ser pascal case.

Los cambios realizados son:
    - Los comentarios innecesarios fueron eliminados.
    - Se extrajo la clase Persona un nuevo archivo.
    - Las funciones de ingreso de datos y generacion del archivo XML fueron movidas a una nueva clase llamada Controlador.
    - Se adecuaron los nombres de las propiedades y metodos de las clases Persona y Controlador.