﻿using SoftwareDesign.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftwareDesign
{
    class Program
    {
        static void Main(string[] args)
        {
            Controlador controlador = new Controlador();

            Persona persona = controlador.IngresarPersona();
            controlador.GenerarXML(persona);
        }
    }

}
