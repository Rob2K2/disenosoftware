﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftwareDesign.Classes
{
    public class Controlador
    {
        public Persona IngresarPersona()
        {
            Persona persona = new Persona();
            string nombre, apellido, ocupacion, edad;
            Console.WriteLine("El objetivo es recolectar los datos del usuario para escribirlos a un archivo XML y luego generar su firma digital");
            Console.WriteLine("Ingrese nombre:");
            nombre = Console.ReadLine();
            Console.WriteLine("Ingrese apellido:");
            apellido = Console.ReadLine();
            Console.WriteLine("Ingrese ocupación:");
            ocupacion = Console.ReadLine();
            Console.WriteLine("Ingrese edad:");
            edad = Console.ReadLine();
            persona.Nombre = nombre;
            persona.Apellido = apellido;
            persona.Ocupacion = ocupacion;
            persona.Edad = Convert.ToInt32(edad);

            return persona;
        }

        public void GenerarXML(Persona persona)
        {
            System.Xml.Serialization.XmlSerializer writer = new System.Xml.Serialization.XmlSerializer(typeof(Persona));

            var path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "//PersonaSerializada.xml";
            System.IO.FileStream file = System.IO.File.Create(path);

            writer.Serialize(file, persona);
            file.Close();
            Console.WriteLine("El archivo XML se generó exitosamente en el escritorio");
            Console.WriteLine("Presione cualquier tecla para continuar");
            Console.ReadKey();
        }
    }
}
